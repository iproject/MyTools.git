package com.taomus.mytools.helper.web.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Perl 风格的正则表达式
 * @author user
 */
@Documented
@Target(FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RegExp {
	String value() default "";
	String message() default "正则匹配错误";
}

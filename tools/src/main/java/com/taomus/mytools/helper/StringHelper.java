package com.taomus.mytools.helper;

import java.util.ArrayList;
import java.util.List;

public class StringHelper {
	public static List<String> split(String src,String regex){
		String[] ss = src.split(regex);
		List<String> strlist = new ArrayList<>();
		for(String str : ss){
			strlist.add(str);
		}
		return strlist;
	}
	
	public static String join(Object[] src,String regex){
		StringBuffer buf = new StringBuffer();
		for(int i=0;i<src.length;i++){
			buf.append(src[i]);
			buf.append(regex);
		}
		buf.deleteCharAt(buf.length()-1);
		return buf.toString();
	}
	
	public static <T> String join(List<T> src,String regex){
		return join(src.toArray(),regex);
	}
	
	public static boolean isEmpty(String src){
		if(null != src && src.trim().equals("")){
			return true;
		}
		return false;
	}
	
	/**
	 * null != src && !src.trim().equals("") ? src : def;
	 * @param src
	 * @param def
	 * @return
	 */
	public static String defaultIfEmpty(String src,String def){
		return null != src && !src.trim().equals("") ? src : def;
	}
	public static boolean isNotEmpty(String src){
		if(null != src  && !src.trim().equals("")){
			return true;
		}
		return false;
	}
}

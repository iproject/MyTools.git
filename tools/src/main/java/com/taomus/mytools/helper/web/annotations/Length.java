package com.taomus.mytools.helper.web.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Length {
	int value() default Integer.SIZE;
	String message() default "长度过长";
	enum Method{LT,LE,GT,GE,EQ,NE};
	Method method() default Method.LE;
}

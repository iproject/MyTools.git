package com.taomus.mytools.helper.properties;


public class Properties {
	
	private static String filename = "mytools.properties";
	
	public static String get(String key){	
		return PropertiesHelper.get(filename, key);
	}
}

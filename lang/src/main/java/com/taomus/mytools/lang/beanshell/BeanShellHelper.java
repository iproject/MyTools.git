package com.taomus.mytools.lang.beanshell;



import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.taomus.mytools.helper.stream.StreamHelper;

import bsh.EvalError;
import bsh.Interpreter;

public class BeanShellHelper{
	private final static Logger logger = LoggerFactory.getLogger(BeanShellHelper.class);
	Interpreter bsh = new Interpreter();
	
	public BeanShellHelper(){}
	public BeanShellHelper(String filename){
		this.load(filename);
	}
	
	public void loadSource(String source){
		try {
			bsh.eval(source);
		} catch (EvalError e) {
			logger.error("BeanShellHelper EvalError ",e);
		}
	}
	
	public void load(String filename){
		try {
			InputStream input = StreamHelper.getInputStreamByFileName(filename);
			String source = StreamHelper.getContent(input);
			this.loadSource(source);
		} catch (IOException e) {
			logger.error("BeanShellHelper IOException ",e);
		}
	}
	
	public BeanShellFied getFied(String name){
		return new BeanShellFied(bsh,name);
	}
}

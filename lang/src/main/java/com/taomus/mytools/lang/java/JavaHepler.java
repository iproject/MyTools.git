package com.taomus.mytools.lang.java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.taomus.mytools.lang.ILanguageHelper;
import com.taomus.mytools.lang.IMethod;

public class JavaHepler extends ILanguageHelper{
	private final static Logger logger = LoggerFactory.getLogger(JavaHepler.class);
	@Override
	public void putPath(String name, String path) {
		if(!this.filePool.containsKey(name)){
			this.filePool.put(name, path);
		}
	}

	@Override
	public IMethod newInstance(String name) throws Exception {
		Class<?> cl  = this.getClass(name);
		logger.debug("实例化类"+cl.getName());
		Object obj = cl.newInstance();
		return new JavaMethod(cl,obj);
	}

	@Override
	public Class<?> getClass(String name) throws Exception {
		String path = null;
		if(name.indexOf(".")!= -1){
			path = name;
		}else{
			path = this.filePool.get(name);
		}	
		return Thread.currentThread().getContextClassLoader().loadClass(path);
	}

}

package com.cn.mytools.test;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import com.taomus.mytools.lang.utils.AsmHelper;
import com.taomus.mytools.lang.utils.AsmHelperOld;
import com.taomus.mytools.lang.utils.AsmMethodHelper;

import org.objectweb.asm.Type;

public class AsmHelperTest {
	public void printType(Object obj){
		String type = obj.getClass().getName();
		System.out.println(type);
		type = Type.getType(type).getDescriptor();
		System.out.println(type);
	}
	//@Test
	public void t(){
		printType(0x1);
	}
	@Test
	public void Asm2Test() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException{
		AsmHelper asm = new AsmHelper("Hello");
		asm.createField("test").Public().value("aaaa").end();
		AsmMethodHelper amh = asm.createMethod("void aa ()").Public().begin();
		amh.putField("test","Hello World").out().getField("test").print().returnValue().end();
		//amh.out().getField("test").print().returnValue().end();
		asm.end();
		Class<?> cl = asm.loadClass2("Hello");
		cl.getMethod("aa").invoke(cl.newInstance());
	}
	//@Test
	public void test() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		AsmHelperOld ah = new AsmHelperOld("Hello");
		ah.createFied("public", "test", 100);
		ah.createMethod("public", "void aa ()");
		ah.setField("test", 100);
		ah.getField("test", "Integer");
		// ah.println(100);
		ah.returnValue();
		ah.end();

		ah.createMethod("public", "void bb ()");
		ah.println("Hello World");
		ah.returnValue();
		ah.end();

		//Class<?> cl = ah.loadClass2("Hello");
		//Object o = cl.newInstance();
		//cl.getMethod("bb").invoke(o);
		// cl.getMethod("aa").invoke(o);
	}
}

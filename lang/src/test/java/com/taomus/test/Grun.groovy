package com.taomus.test;

import com.taomus.mrc.mytools.lang.IInvoke;
import com.taomus.mrc.mytools.lang.IMethod;
import com.taomus.mrc.mytools.lang.clojure.ClojureHelper;

class Grun{
	def populationlist=["(if #0 #1 #2)","(> #0 #1)","(str #0)",
	                    "(< #0 #1)","(and #0 #1)","(or #0 #1)",
	                    "(not #0)","(+ #0 #1)","(- #0 #1)",
	                    "(* #0 #1)","(/ #0 #1)","(% #0 #1)",
	                    "(<= #0 #1)","(>= #0 #1)","(= #0 #1)",
	                    "(def #0 #1)","(do #0)","(map inc #l)",
	                    "(map dec #l)","(reduce + #l)","(reduce - #l)",
	                    "(reduce * #l)","(reduce / #l)","(filter even? #l)"
	                    ];
	def run(){
		population()
		def str ="(reduce * (range 1 (inc 5)))";
		eval(str)
	}
	def eval(String script){
		println(ClojureHelper.eval(script));
	}
	def population(){
		println populationlist.join(",");
	}
	/**
	 * 变异
	 */
	def mutate(){
		
	}
	/**
	 * 交叉
	 */
	def crossover(){
		
	}
	
	def write(String content){
		
	}	
}
package com.taomus.mytools.helper.xml.annotations;

import static java.lang.annotation.ElementType.FIELD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Attribute {
	String value() default "";
}

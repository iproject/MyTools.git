package com.cn.mytools.test;

import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import com.taomus.mytools.helper.MappingHelper;
import com.taomus.mytools.helper.ObjectHelper;
import com.taomus.mytools.helper.PageHelper;
import com.taomus.mytools.helper.StringHelper;
import com.taomus.mytools.helper.bean.ShowBeanHelper;
import com.taomus.mytools.helper.web.VerifyHelper;
import com.taomus.mytools.lang.utils.AsmHelperOld;

public class Test2{
	
	//@Test
	public void tstt() throws IllegalArgumentException, IllegalAccessException{
		T1 t1 = new T1();
		//t1.setA("aa");
		t1.setB(123);
		VerifyHelper vh  = new VerifyHelper(t1);
		System.out.println(vh.verify(null));
		System.out.println("end");
	}
	
	//@Test
//	public void tt() throws InstantiationException, IllegalAccessException{
//		T1 t1 = new T1();
//		Map<String,T1> tl = new HashMap<>();
//		tl.put("aa", t1);
//		Map<String,T2> t2 = (Map<String,T2>) MappingHelper.mapping(tl, T2.class);
//		System.out.println(t2.get("aa").getA());
//	//	System.out.println(VerifyHelper.verify(t1));
//	}
//	@Test
	public void tDefaultIfNull(){
		String src = "Hello src";
		String i = ObjectHelper.defaultIfNull(src,"Hello World");
		System.out.println(i);
	}
	//@Test
	public void tJoin(){
		Object[] a = new Object[]{1,2};
		List<String> sl = Arrays.asList("a","c");
		System.out.println(StringHelper.join(a,","));
		System.out.println(StringHelper.join(sl,"$"));
		System.out.println(StringHelper.join(a,"#"));
	}
	
	
	//@Test
	public void showBean(){
		class obj{
			public Map<Object,Object> objMap = new HashMap<Object,Object>();
		}
		obj o = new obj();
		o.objMap.put("aa", "bb");
		System.out.println(o.objMap.get("aa"));
		//ShowBeanHelper.show(o);
	}

//	@Test
	public void pageTest(){
		int total = 100;
		int row = 5;
		int curPage=3;
		PageHelper pageHelper = new PageHelper();
		PageHelper.Page page = pageHelper.mySqlLimitPage(total,row,curPage);
		System.out.println(page.toString());

	//	page = pageHelper.oraclePage(total,row,curPage);
		System.out.println(page.toString());
	}

    /*@Test
    public void t() throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException{
    	AsmHelperOld ah = new AsmHelperOld("Hello");
    	ah.createFied("public","test",100);
    	ah.createMethod("public","void aa ()");
    	ah.setField("test", 100);
    	ah.getField("test","Integer");
    	//ah.println(100);
    	ah.returnValue();
    	ah.end();
    	
    	ah.createMethod("public","void bb ()");
    	ah.println("Hello World");
    	ah.returnValue();
    	ah.end();
    	
    	Class<?> cl = ah.loadClass2("Hello");
    	Object o = cl.newInstance();
    	cl.getMethod("bb").invoke(o);
    	//cl.getMethod("aa").invoke(o);
    }*/
}

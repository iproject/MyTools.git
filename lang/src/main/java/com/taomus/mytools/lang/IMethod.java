package com.taomus.mytools.lang;

public interface IMethod {
	/**
	 * 获取要调用的方法
	 * @param name
	 * @return
	 */
	IInvoke getMethod(String name);
}

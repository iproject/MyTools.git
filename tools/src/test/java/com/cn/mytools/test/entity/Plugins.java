package com.cn.mytools.test.entity;

import java.util.List;

import com.taomus.mytools.helper.xml.annotations.Element;

@Element
public class Plugins {
	@Element
	private List<Plugin> plugin;
	
	public List<Plugin> getPlugin() {
		return plugin;
	}

	public void setPlugins(List<Plugin> plugin) {
		this.plugin = plugin;
	}

}

(ns com.taomus.mrc.mytools.lang.clojure.script.CljEval)

(defn cljEval [value]
	(eval (read-string value))
)

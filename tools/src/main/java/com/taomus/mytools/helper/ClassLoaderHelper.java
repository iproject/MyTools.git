package com.taomus.mytools.helper;

public class ClassLoaderHelper {
	public static Class<?> load(String packageName) throws ClassNotFoundException{
		return Thread.currentThread().getContextClassLoader().loadClass(packageName);
	}
}

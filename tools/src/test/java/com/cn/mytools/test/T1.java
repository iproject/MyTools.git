package com.cn.mytools.test;

import com.taomus.mytools.helper.web.annotations.Length;
import com.taomus.mytools.helper.web.annotations.NotEmpty;
import com.taomus.mytools.helper.web.annotations.NotNull;
import com.taomus.mytools.helper.web.annotations.RegExp;
import com.taomus.mytools.helper.web.annotations.Length.Method;

public class T1 {
	
	@NotNull(message="用户名不能为空")
	private String a=null;
	
	@Length(value=3,method=Method.EQ)
	@RegExp("/[0-9]{3}/")
	private Integer b = null;

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public Integer getB() {
		return b;
	}

	public void setB(Integer b) {
		this.b = b;
	}
	
}

package com.taomus.mytools.helper.other;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 穆仁超
 */
public class PingHelper {
	public static List<String> ping(String ip){
		return ping(ip,"");
	}
	/**
	 * @param ip
	 * @param argv
	 * @return
	 */
	public static List<String> ping(String ip,String argv){
		String osname = CommandUtils.getOSName();
        List<String> pingList = new ArrayList<String>();
        Process pro = null;
        String line = null;
        try {
            if(osname.startsWith("Windows")){
                pro = Runtime.getRuntime().exec("cmd /c ping -n "+argv+" "+ip);
            
            }else if(osname.startsWith("Linux")){
                pro = Runtime.getRuntime().exec("ping -c "+argv+" "+ip);
            }
            BufferedReader buf = new BufferedReader(new InputStreamReader(pro.getInputStream()));
            while((line = buf.readLine())!=null){
                pingList.add(line);
            }
            return pingList;
        }catch(Exception ex){}
        return null;
	}
}

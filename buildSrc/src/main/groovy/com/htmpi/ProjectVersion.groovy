package com.htmpi;

class ProjectVersion{
    Integer major
    Integer minor
	Integer patch
    Boolean release
    ProjectVersion(Integer major,Integer minor,Integer patch){
        this.major = major
        this.minor = minor
		this.patch = patch
        this.release = false
    }
    ProjectVersion(Integer major,Integer minor,Integer patch,Boolean release){
        this(major,minor,patch)
        this.release = release
    }
    @Override
    String toString(){
		def date = new Date().format("yyyyMMddHHmmss")
        "$major.$minor.$patch${release ? '-'+date : '-'+date+'-SNAPSHOT'}"
    }
}

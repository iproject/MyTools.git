package com.taomus.mytools.helper.stream;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScanningFileHelper {

	private final static Logger logger = LoggerFactory
			.getLogger(ScanningFileHelper.class);
	private Set<String> fileSet = new HashSet<String>();

	public Set<String> getFileSet() {
		return fileSet;
	}

	public void scanning(String path) {
		if (path != null && !path.equals("")) {
			File file = new File(path);
			File[] childFiles = file.listFiles();
			for (File childFile : childFiles) {
				if (childFile.isDirectory()) {
					scanning(childFile.getPath());
				} else {
					String childFilePath = childFile.getPath();
					logger.info(childFilePath);
					fileSet.add(childFilePath);
				}
			}
		}
	}

	@SuppressWarnings("resource")
	public Set<String> getJarClassPath(File file) {
		Set<String> paths = new HashSet<String>();
		try {
			if (file.getPath().endsWith(".jar")) {
		/*		long t = file.lastModified();
				DateHelper d = new DateHelper();
				String s = d.DateToString(d.timestampToDate(t));*/
				JarFile jfile = new JarFile(file);
				Enumeration<JarEntry> files = jfile.entries();
				while (files.hasMoreElements()) {
					JarEntry entry = (JarEntry) files.nextElement();
					paths.add(entry.getName());
					logger.info(entry.getName());
				}
			}
		} catch (IOException e) {
			logger.error("",e);
		}
		return paths;
	}
	
	@SuppressWarnings("resource")
	public String getManifest(File file,String key){
		try {
			JarFile jfile = new JarFile(file);
			Manifest mf = jfile.getManifest();
			Attributes attr = mf.getMainAttributes();
			if(attr.containsKey(key)){
				return attr.getValue(key);
			}
			return null;
		} catch (IOException e) {
			logger.error("",e);
		}
		return null;
	}
	@Test
	public void testManIfeset() throws IOException{
		File j = new File("C:/test.jar");
		JarFile f  = new JarFile(j);
		Manifest mf = f.getManifest();
		String val = mf.getMainAttributes().getValue("Manifest-Version");
		System.out.println(val);
	}
}

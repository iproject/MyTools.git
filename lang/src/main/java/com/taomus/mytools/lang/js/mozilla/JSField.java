package com.taomus.mytools.lang.js.mozilla;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JSField {
	private final static Logger logger = LoggerFactory.getLogger(JSHelper.class);
	
	private Scriptable scope;
	private String fieldName;
	
	public JSField(String inFieldName,Scriptable inScope){
		this.scope = inScope;
		this.fieldName = inFieldName;
	}
	
	public void set(Object val){
		scope.put(fieldName, scope, val);
	}
	
	public void javaToJs(Object value){
		Object obj = Context.javaToJS(value, scope);
		ScriptableObject.putProperty(scope, fieldName, obj);
	}
	
	public String get(){
		Object obj = scope.get(fieldName, scope);
		if(obj == Scriptable.NOT_FOUND){
			Context.exit();
			logger.debug("not found");
			return null;
		}
		return Context.toString(obj);
	}
	
	public void exit(){
		Context.exit();
	}


}

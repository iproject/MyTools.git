package com.taomus.mytools.helper.xml.annotations;

import static java.lang.annotation.ElementType.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({FIELD,TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Element {
	String value() default "";
}

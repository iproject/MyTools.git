package com.taomus.mytools.lang;

public interface IInvoke {
	/**
	 * 调用无参数的方法
	 * @return
	 */
	Object invoke();
	/**
	 * 调用带参数的方法
	 * @param args
	 * @return
	 */
	Object invoke(Object ...args);
	
}

package com.taomus.mytools.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author 穆仁超
 */
public class DateHelper {
	/**
	 * 获取当前时间到24时的秒数
	 * @return
	 */
	public long nowBetween24(){
		 LocalDateTime midnight = LocalDateTime.now().plusDays(1).withHour(0).withMinute(0).withSecond(0)
                 .withNano(0);
         long seconds = ChronoUnit.SECONDS.between(LocalDateTime.now(), midnight);
         return seconds;
	}
	
	public Date toDate(long timestamp){
		return new Date(timestamp);
	}
	
	/**
	 * Date 转换成 String
	 * @param format
	 * @param date
	 * @return
	 */
	public String toString(String format,Date date){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	/**
	 * Date 转换成 String
	 * yyyy-MM-dd HH:mm:ss
	 * @param date
	 * @return
	 */
	public String toString(Date date){
		return this.toString("yyyy-MM-dd HH:mm:ss", date);
	}
	
	/**
	 * 系统当前时间转为字符串
	 * @return
	 */
	public String nowToStr(String formath){
		return this.toString(formath,new Date());
	}
	
	/**
	 * String 转换成 Date
	 * @param format yyyy-MM-dd HH:mm:ss
	 * @param source 2015-12-31	23:59:59
	 * @return Date
	 * @throws ParseException
	 */
	public Date toDate(String format,String source) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(source);
	}
	
	/**
	 * String 转换成 Date
	 * @param source 2015-12-31 23:59:59
	 * @return Date
	 * @throws ParseException
	 */
	public Date toDate(String source) throws ParseException{
		return this.toDate("yyyy-MM-dd HH:mm:ss", source);
	}
}

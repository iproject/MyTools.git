package com.taomus.mytools.helper.bean;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public class ShowBeanHelper {

	public static void show(Object obj) {
		try {
			for (Field field : obj.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				Object value = field.get(obj);
				if(value == null) continue;
				if(value instanceof String){
					System.out.println(field + "==>" + value);
				} else if(value instanceof Integer || value.equals(int.class)){
					System.out.println(field + "==>" + value);
				}else if(value instanceof List){
					for(Object pobj : (List<?>)value){
						show(pobj);
						System.out.println("-------------------------------------------");
					}
				}else if(value instanceof Map){
					Map<?,?> omap = (Map<?,?>)value;
					for(Object key : omap.keySet()){
						show(omap.get(key));
						System.out.println("-------------------------------------------");
					}
				}else{
					show(value);
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}

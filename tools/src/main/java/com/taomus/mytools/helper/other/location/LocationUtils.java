package com.taomus.mytools.helper.other.location;

/**
 * 摘抄google maps
 * @author 穆仁超
 *
 */
public class LocationUtils {
	private final static double EARTH_RADIUS = 6378.137;
	private static double rad(double d){
		return d * Math.PI / 180.0;
	}
	
	public static double GetDistance(double lat1,double lon1,double lat2,double lon2){
		double radLat1 = rad(lat1);
		double radLat2 = rad(lat2);
		double a  = radLat1 -radLat2;
		double b = rad(lon1) - rad(lon2);
		double s =2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) +
			    Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
		s *= EARTH_RADIUS;
		s = Math.round(s * 10000) / 10000.0;
		return s;
		
	}
	
	public static void main(String[] args){
		double lat1=40.174674;
		double lon1=116.984898;
		double lat2=40.175674;
		double lon2=116.984898;
		System.out.println(LocationUtils.GetDistance(lat1, lon1, lat2, lon2)+"km");
	}
}

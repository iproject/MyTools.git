package com.taomus.mytools.helper;

/**
 * Created by mrc on 17-1-1.
 */
public class PageHelper {
    public static class Page {
        private int totalPage;
        private int rowNum;
        private int curPage;
        private int startPage;
        private int endPage;

        @Override
        public String toString() {
            return "Page{" +
                    "totalPage=" + totalPage +
                    ", rowNum=" + rowNum +
                    ", curPage=" + curPage +
                    ", startPage=" + startPage +
                    ", endPage=" + endPage +
                    '}';
        }

        public int getTotalPage(){
            return this.totalPage;
        }
        public void setTotalPage(int total){
            this.totalPage = total;
        }
        public int getRowNum(){
            return this.rowNum;
        }
        public void setRowNum(int rowNum){
            this.rowNum = rowNum;
        }
        public int getCurPage(){
            return this.curPage;
        }
        public void setCurPage(int curPage){
            this.curPage = curPage;
        }

        public int getStartPage() {
            return startPage;
        }

        public void setStartPage(int startPage) {
            this.startPage = startPage;
        }

        public int getEndPage() {
            return endPage;
        }

        public void setEndPage(int endPage) {
            this.endPage = endPage;
        }
    }

    public Page mySqlLimitPage(int total,int rowNum,int curPage){
        int totalPage = total / rowNum ;
        totalPage += total % rowNum > 0 ? 1 : 0;
        Page page = new Page();
        page.setTotalPage(totalPage);
        page.setRowNum(rowNum);
        page.setStartPage((curPage-1)*rowNum);
        page.setCurPage(curPage);
        return page;
    }

    public Page oracleRowNumPage(int total,int rowNum,int curPage){
        int totalPage = total / rowNum ;
        totalPage += total % rowNum > 0 ? 1 : 0;
        Page page = new Page();
        page.setTotalPage(totalPage);
        page.setStartPage((curPage-1)*rowNum);
        page.setEndPage((curPage-1)*rowNum+rowNum);
        page.setCurPage(curPage);
        page.setRowNum(rowNum);
        return page;
    }

    public Page dB2RowIdPage(int total,int rowNum,int curPage){
        return this.oracleRowNumPage(total,rowNum,curPage);
    }

    public Page sqllitePage(int total,int rowNum,int curPage){
       return this.mySqlLimitPage(total, rowNum, curPage);
    }

}

package com.taomus.mytools.helper.other;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

public class CommandUtils {
	public static String join(String[] args,String regx){
		String res = "";
		for(String str : args){
			res = str + regx;
		}
		return res.substring(0,res.lastIndexOf(regx));
	}
	
	public static String getOSName(){
		return System.getProperty("os.name");
	}
	
	public static Boolean isOSWindows(){
		String osname = System.getProperty("os.name");
		if(osname.startsWith("Windows")){
			return true;
		}
		return false;
	}
	
	public static Boolean isOSLinux(){
		String osname = System.getProperty("os.name");
		if(osname.startsWith("Linux")){
			return true;
		}
		return false;
	}
	
	public static BufferedReader excute(String cmd) throws IOException{
		  String osname = getOSName();
		  Process pro  = null;
		  if(osname.startsWith("Windows")){
              pro = Runtime.getRuntime().exec("cmd /c "+cmd);
          
          }else if(osname.startsWith("Linux")){
              pro = Runtime.getRuntime().exec(cmd);
          }
          BufferedReader buf = new BufferedReader(new InputStreamReader(pro.getInputStream()));
          return buf;
	}
	
	@Test
	public void t(){
		System.out.print(getOSName());
	}
}

package com.taomus.mytools.helper.web.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target(FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NotNull {
	String value() default "";
	String message() default "不能为空";
}

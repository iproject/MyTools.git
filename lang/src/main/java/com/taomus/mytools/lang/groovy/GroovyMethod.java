package com.taomus.mytools.lang.groovy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.taomus.mytools.lang.IInvoke;
import com.taomus.mytools.lang.IMethod;

import groovy.lang.GroovyObject;

public class GroovyMethod implements IMethod{
	private GroovyObject obj = null;

	public GroovyMethod(GroovyObject obj) {
		this.obj = obj;
	}
	
	@Override
	public IInvoke getMethod(String name) {
		return new GroovyInvoke(name, obj);
	}

	public static class GroovyInvoke implements IInvoke{
		private static Logger logger = LoggerFactory.getLogger(IInvoke.class);
		private GroovyObject obj = null;

		private String name = null;

		public GroovyInvoke(String name, GroovyObject obj) {
			this.obj = obj;
			this.name = name;
		}

		public Object invoke(Object... args) {
			logger.debug("invoke "+this.name);
			return obj.invokeMethod(this.name, args);
		}

		public Object invoke() {
			logger.debug("invoke "+this.name);
			return obj.invokeMethod(this.name, null);
		}
	}

}
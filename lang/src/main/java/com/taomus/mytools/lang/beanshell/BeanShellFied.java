package com.taomus.mytools.lang.beanshell;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import bsh.EvalError;
import bsh.Interpreter;

public class BeanShellFied {
	private final static Logger logger = LoggerFactory.getLogger(BeanShellFied.class);
	private Interpreter bsh;
	private String name;

	public BeanShellFied(Interpreter bsh,String name) {
		this.bsh = bsh;
		this.name = name;
	}
	
	public void set(Object value){
		try {
			bsh.set(this.name,value);
		} catch (EvalError e) {
			logger.error("BeanShellFied EvalError ",e);
		}
	}
	
	public Object get(){
		try {
			return bsh.get(this.name);
		} catch (EvalError e) {
			logger.error("BeanShellFied EvalError ",e);
		}
		return null;
	}
}

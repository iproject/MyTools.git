package com.taomus.mytools.lang;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class ILanguageHelper {

	protected final Map<String,String> filePool = new LinkedHashMap<String,String>();
	protected String classPath = "";
	protected ClassLoader classLoader=null;
	
	public void setClassLoader(final ClassLoader inClassLoader){
		this.classLoader = inClassLoader;
	}
	
	/**
	 * 设置类文件路径
	 * @param classpath
	 */
	public void setClassPath(final String classpath){
		this.classPath = classpath;
	}
	/***
	 * 设置语言文件路径
	 * @param name
	 * @param path
	 */
	public abstract void putPath(String name,String path);
	/**
	 * 实例化对象
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public abstract IMethod newInstance(String name) throws Exception;
	/**
	 * 获取文件的class (不是所有脚本语言都支持)
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public abstract Class<?> getClass(String name) throws Exception;
}

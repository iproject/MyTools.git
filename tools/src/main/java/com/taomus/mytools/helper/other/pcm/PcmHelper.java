package com.taomus.mytools.helper.other.pcm;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


public class PcmHelper {
	public static void toWave(String src,String desc) throws IOException{
		FileInputStream fis = new FileInputStream(src);
		FileOutputStream fos = new FileOutputStream(desc);
		byte[] buf = new byte[1024 * 4];
		int size = fis.read(buf);
		int pcmSize = 0;
		while(size != -1){
			pcmSize += size;
			size = fis.read(buf);
		}
		fis.close();
		WaveHeader header = new WaveHeader(pcmSize);
		byte[] h = header.getHeader();
		fos.write(h,0,h.length);
		fis = new FileInputStream(src);
		byte[] c = new byte[pcmSize];
		fis.read(c);
		fos.write(c,0,pcmSize);
		fis.close();
		fos.close();
		System.out.println("Convert OK!");
	}
}

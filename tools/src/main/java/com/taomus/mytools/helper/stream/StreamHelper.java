package com.taomus.mytools.helper.stream;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.taomus.mytools.helper.OSHelper;

public class StreamHelper {
	private InputStream input;

	public StreamHelper() {
	}

	public StreamHelper(String name) {
		String path = this.getFilePath(name);
		this.input = this.getInputStream(path);
	}

	public String getFilePath(String name) {
		return Thread.currentThread().getContextClassLoader().getResource(name).getPath();
	}

	public InputStream getInputStream(String name) {
		return Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
	}

	public String getContent() throws IOException {
		return getContent(input);
	}

	public static String getContent(InputStream input) throws IOException {
		return getContent(input, "UTF-8");
	}

	public static String getContent(InputStream input, String encoding) throws IOException {
		StringBuilder sb = new StringBuilder();
		InputStreamReader isr = new InputStreamReader(input, encoding);
		BufferedReader br = new BufferedReader(isr);
		int cline;
		while ((cline = br.read()) != -1) {
			sb.append((char)cline);
		}
		isr.close();
		br.close();
		input.close();
		return sb.toString();
	}

	public static byte[] getByteContent(InputStream input) throws IOException {
		ByteArrayOutputStream br = new ByteArrayOutputStream();
		byte[] buf = new byte[4096];
		int size;
		while ((size = input.read(buf, 0, buf.length)) != -1) {
			br.write(buf, 0, size);
			br.flush();
		}
		br.close();
		input.close();
		return br.toByteArray();
	}

	public static void save(String path, byte[] content) throws IOException {
		FileOutputStream output = new FileOutputStream(new File(path));
		output.write(content, 0, content.length);
		output.flush();
		output.close();
	}
	
	public static void save(String path, String content) throws IOException {
		save(path,new ByteArrayInputStream(content.getBytes()));
	}
	
	public static void SaveBase64(String content){
		
	}

	public static void save(String path, InputStream input) throws IOException {
		FileOutputStream output = new FileOutputStream(new File(path));
		byte[] buf = new byte[4096];
		int size;
		while ((size = input.read(buf, 0, buf.length)) != -1) {
			output.write(buf, 0, size);
			output.flush();
		}
		output.close();
		input.close();
	}

	public static InputStream getInputStreamByFileName(String name) {
		InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
		if (input == null) {
			String path = System.getProperty("user.dir");
			try {
				input = new FileInputStream(new File(path + "/" + name));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		return input;
	}
	
	public static InputStream getJarFileInputStream(String jarPath,String path) throws IOException{
		StringBuffer jarPathSb = new StringBuffer("jar:file:");
		if(OSHelper.isWindows()){
			jarPathSb.append("/");
		}
		jarPathSb.append(jarPath);
		jarPathSb.append("!/");
		jarPathSb.append(path);
		URL url = new URL(jarPathSb.toString());
		JarURLConnection jarConnection = (JarURLConnection)url.openConnection();
		return jarConnection.getInputStream();
	}

	public static String getFilePathByFileName(String name) {
		return Thread.currentThread().getContextClassLoader().getResource(name).getPath();
	}

	public static void main(String[] args) {
		System.out.println(Thread.currentThread().getContextClassLoader().getResource(".").getPath());
	}

	public InputStream getJarInputStreamByFileName(String name) {
		return this.getClass().getResourceAsStream(name);
	}

}

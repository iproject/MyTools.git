package com.taomus.mytools.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.taomus.mytools.date.ntp.DateNTPHelper;

/**
 * NTP Date
 * @author 穆仁超
 */
public class Date extends java.util.Date{
	public static void main(String[] args){
			Date d = new Date();
			System.out.println(d.getTime());
			System.out.println(d.getUnixTime());
	}
	private static final long serialVersionUID = 1L;
	public Date(){
		super(DateNTPHelper.currentTimeMillis());
	}
	
	public Date(long date){
		super(date);
	}
	
	public long getUnixTime(){
		return this.getTime()/1000;
	}
	
	public String toString(){
		return this.toString("yyyy-MM-dd HH:mm:ss",this);
	}
	public String toString(String format){
		return this.toString(format,this);
	}
	public String toString(String format,java.util.Date date){
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	/**
	 * String 转换成 Date
	 * @param format yyyy-MM-dd HH:mm:ss
	 * @param source 2015-12-31	23:59:59
	 * @return Date
	 * @throws ParseException
	 */
	public java.util.Date toDate(String format,String source) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(source);
	}
	
	/**
	 * String 转换成 Date
	 * @param source 2015-12-31 23:59:59
	 * @return Date
	 * @throws ParseException
	 */
	public java.util.Date toDate(String source) throws ParseException{
		return this.toDate("yyyy-MM-dd HH:mm:ss", source);
	}
}

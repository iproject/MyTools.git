package com.taomus.mytools.lang.clojure;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.taomus.mytools.lang.ILanguageHelper;
import com.taomus.mytools.lang.IMethod;

import clojure.lang.RT;

public class ClojureHelper extends ILanguageHelper {
	private static Logger logger = LoggerFactory.getLogger(ClojureHelper.class);
	
	public static Object eval(String script){
		ClojureHelper ch = new ClojureHelper();
		IMethod im = ch.newInstance("com/taomus/mytools/lang/clojure/script/CljEval.clj");
		return im.getMethod("cljEval").invoke(script);
	}
	
	@Override
	public void putPath(String name, String path) {
		if(!this.filePool.containsKey(name)){
			logger.debug(path);
			if(path.endsWith(".clj")){
				path  = path.replace(".clj","");
			}
			this.filePool.put(name, path);
		}
	}
	
	@Override
	public IMethod newInstance(String name){
		String file = null;
		if(name.endsWith(".clj")){
			file = name.replace(".clj", "");
		}else{
			file = this.filePool.get(name);
		}
		try {
			RT.load(file);
		} catch (ClassNotFoundException e) {
			logger.error("ClojureHelper",e);
		} catch (IOException e) {
			logger.error("ClojureHelper",e);
		}
		return new ClojureMethod(file.replace("/", ".").replace("_", "-"));
	}
	
	@Override
	public Class<?> getClass(String name) throws Exception {
		return null;
	}
}

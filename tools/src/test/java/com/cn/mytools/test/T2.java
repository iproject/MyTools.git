package com.cn.mytools.test;

import com.taomus.mytools.helper.annotations.Wrapper;

public class T2 {
	
	@Wrapper(value=W.class)
	private String a;

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}
}

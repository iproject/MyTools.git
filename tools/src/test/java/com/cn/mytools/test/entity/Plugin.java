package com.cn.mytools.test.entity;

import com.taomus.mytools.helper.xml.annotations.Attribute;
import com.taomus.mytools.helper.xml.annotations.Element;

public class Plugin {
	
	@Element("name")
	private String name;
	@Element("class")
	private String classPath;
	@Attribute("type")
	private String type;
	
	@Element
	private Configure test;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getClassPath() {
		return classPath;
	}
	public void setClassPath(String classPath) {
		this.classPath = classPath;
	}
	@Override
	public String toString() {
		return "Plugin [name=" + name + ", classPath=" + classPath + ", type=" + type + "]";
	}

}
